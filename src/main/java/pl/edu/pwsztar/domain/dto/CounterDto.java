package pl.edu.pwsztar.domain.dto;

public class CounterDto {
    private int counter;

    public CounterDto() {
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
